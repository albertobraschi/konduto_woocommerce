<?php
/**
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'konduto_wordpress');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '123');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'V`~e(YH|Z*)x8)tV $*]0Wfyx@G{S]|nK5v>6Zm`kXun%:L+_Pm$rmvDf9t1u$Te');
define('SECURE_AUTH_KEY',  '.]~$@17g#(S,o_4:#$&+.z/}M|W !#`M&[+p(FIhq[I7&c/EC@6*CH`*PU#y:kYT');
define('LOGGED_IN_KEY',    'liDgc[RbW;58h3E53<|l]quMsPoFBBVvd[(]TEEcA>U3F)_0XU+}qlR+gOvM2;(X');
define('NONCE_KEY',        'o1}vUZz&86>B:h^E7{*&b^o:[HY<Q7{~.E<rJ|z,O:_S9K5+Nw@>y{9+zD},mKK/');
define('AUTH_SALT',        '^gfSS<Wh7^}ZfOrh8{`wm4y+KYF/<Q| ||E5)FX~cW1~9w/p}_H`IQ@9>/|*v$`$');
define('SECURE_AUTH_SALT', 'xP/^@r|M0Q&_FHhsbd|Q:>SO[{rs-HS|jRTx7</-I<1VyX=^qEgWngmLyr!6Y]u*');
define('LOGGED_IN_SALT',   'R4aqhTR-&$JTU|5sLQa%&06eD~r+S+Tan_sZVDl@tpX^mfHG|c/j1N*l-S;?ALVz');
define('NONCE_SALT',       'MK3-<Wc*QPl 8R9:<W0&Xr,aCgw5^~zE:Ob5G^$C6=$`H-oEP*d5m:L;h_g2i-jR');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';


/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
