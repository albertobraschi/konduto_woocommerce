<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

if (!class_exists('WC_Konduto_Settings')) {

    class WC_Konduto_Settings {

        const SETTINGS_NAMESPACE = 'wc_konduto';

        /**
         * Get the setting fields
         *
         * @since  1.0.0
         * @access private
         *
         * @return array $setting_fields
         */
        private function get_fields() {



            $setting_fields = array(
                'section_title' => array(
                    'name' => __('Konduto - Configurações de Antifraude', 'woocommerce-konduto'),
                    'type' => 'title',
                    'desc' => '',
                    'id' => 'wc_settings_' . self::SETTINGS_NAMESPACE . '_title'
                ),
                'environment_konduto' => array(
                    'name' => __('Ambiente', 'woocommerce-konduto'),
                    'type' => 'select',
                    'options' => array("production" => "Produção", "homolog" => "Teste"),
                    'id' => 'wc_settings_' . self::SETTINGS_NAMESPACE . '_environment',
                    'description' => "Alternar entre os modos de Teste e Produção",
                    'desc_tip' => true
                ),
                'privkey_konduto' => array(
                    'name' => __('Chave Privada', 'woocommerce-konduto'),
                    'type' => 'text',
                    'id' => 'wc_settings_' . self::SETTINGS_NAMESPACE . '_privkey',
                    'description' => "TXXXXXXXXXXXXXXXXXXXX (21 caracteres)",
                    'desc_tip' => true
                ),
                'publickey_konduto' => array(
                    'name' => __('Chave Pública', 'woocommerce-konduto'),
                    'type' => 'text',
                    'id' => 'wc_settings_' . self::SETTINGS_NAMESPACE . '_publickey',
                    'description' => "TXXXXXXXXXX (11 caracteres)",
                    'desc_tip' => true
                )
            );
            /**
             * Filter: 'wc_settings_tab_anti_fraud' - Allow altering extension setting fields
             *
             * @api array $setting_fields The fields
             */
            return apply_filters('wc_settings_tab_' . self::SETTINGS_NAMESPACE, $setting_fields);
        }

        /**
         * Get an option set in our settings tab
         *
         * @param $key
         *
         * @since  1.0.0
         * @access public
         *
         * @return String
         */
        public function get_option($key) {
            $fields = $this->get_fields();

            /**
             * Filter: 'wc_settings_$key' - Allow altering one option
             *
             * @api array $value The option value
             */
            return apply_filters('wc_option_' . $key, get_option('wc_settings_' . self::SETTINGS_NAMESPACE . '_' . $key, ( ( isset($fields[$key]) && isset($fields[$key]['default']) ) ? $fields[$key]['default'] : '')));
        }

        /**
         * Setup the WooCommerce settings
         *
         * @since  1.0.0
         * @access public
         */
        public function setup() {
            add_filter('woocommerce_settings_tabs_array', array($this, 'add_settings_tab'), 70);
            add_action('woocommerce_settings_tabs_' . self::SETTINGS_NAMESPACE, array($this, 'tab_content'));
            add_action('woocommerce_update_options_' . self::SETTINGS_NAMESPACE, array($this, 'update_settings'));
        }

        /**
         * Add a settings tab to the settings page
         *
         * @param array $settings_tabs
         *
         * @since  1.0.0
         * @access public
         *
         * @return array
         */
        public function add_settings_tab($settings_tabs) {
            $settings_tabs[self::SETTINGS_NAMESPACE] = __('Konduto', 'woocommerce-konduto');

            return $settings_tabs;
        }

        /**
         * Output the tab content
         *
         * @since  1.0.0
         * @access public
         *
         */
        public function tab_content() {
            woocommerce_admin_fields($this->get_fields());
        }

        /**
         * Update the settings
         *
         * @since  1.0.0
         * @access public
         */
        public function update_settings() {
            woocommerce_update_options($this->get_fields());
        }

    }

}
