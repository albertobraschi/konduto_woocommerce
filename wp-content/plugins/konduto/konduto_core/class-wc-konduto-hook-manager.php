<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

if (!class_exists('WC_Konduto_Hook_Manager')) {

    class WC_Konduto_Hook_Manager {

        private static $instance = null;

        /**
         * Private constructor, initiate class via ::setup()
         */
        private function __construct() {
            // Meta Boxes
            add_action('add_meta_boxes', array($this, 'add_meta_boxes'));
        }

        /**
         * The setup method // singleton initiator
         *
         * @static
         * @since  1.0.0
         * @access public
         */
        public static function setup() {
            if (null === self::$instance) {
                self::$instance = new self();
            }
        }

        /**
         * Add the meta boxes
         *
         * @since  1.0.0
         * @access public
          public function add_meta_boxes() {
          new WC__Meta_Box();
          }

         */
    }

}
