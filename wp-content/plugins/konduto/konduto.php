<?php
/*
  Plugin Name: Konduto

  Description: Konduto
  Version: 1.0.0
  Author: Koan Commerce - Alberto Braschi
  Author URI: http://www.turbinesualoja.biz

 */
if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

/**
 * Required functions
 */
if (!function_exists('woothemes_queue_update')) {
    require_once( plugin_dir_path(__FILE__) . '/woo-includes/woo-functions.php' );
}

/**
 * Plugin updates
 */
woothemes_queue_update(plugin_basename(__FILE__), '955da0ce83ea5a44fc268eb185e46c41', '500217');

class WooCommerce_Konduto {

    /**
     * Get the plugin file
     *
     * @static
     * @since  1.0.0
     * @access public
     *
     * @return String
     */
    public static function get_plugin_file() {
        return __FILE__;
    }

    /**
     * A static method that will setup the autoloader
     *
     * @static
     * @since  1.0.0
     * @access private
     */
    private static function setup_autoloader() {
        require_once( plugin_dir_path(self::get_plugin_file()) . '/konduto_classes/konduto_autoloader.php' );

// Core loader
        $core_autoloader = new Konduto_Autoloader(plugin_dir_path(self::get_plugin_file()) . 'konduto_core/');
        spl_autoload_register(array($core_autoloader, 'load'));
    }

    /**
     * Constructor
     */
    public function __construct() {
// Check if WC is activated
        if ($this->is_wc_active()) {
            $this->init();
        }
    }

    /**
     * Check if WooCommerce is active
     *
     * @since  1.0.0
     * @access public
     *
     * @return bool
     */
    private function is_wc_active() {

        $is_active = WC_Dependencies::woocommerce_active_check();

// Do the WC active check
        if (false === $is_active) {
            add_action('admin_notices', array($this, 'notice_activate_wc'));
        }

        return $is_active;
    }

    /**
     * Display the notice
     *
     * @since  1.0.0
     * @access public
     *
     */
    public function notice_activate_wc() {
        ?>
        <div class="error">
            <p><?php printf(__('Please install and activate %sWooCommerce%s in order for the WooCommerce Anti Fraud extension to work!', 'woocommerce-anti-fraud'), '<a href="' . admin_url('plugin-install.php?tab=search&s=WooCommerce&plugin-search-input=Search+Plugins') . '">', '</a>'); ?></p>
        </div>
        <?php
    }

    /**
     * Init the plugin
     *
     * @since  1.0.0
     * @access private
     *
     */
    private function init() {

// Setup the autoloader
        self::setup_autoloader();

// Setup the required WooCommerce hooks
        WC_Konduto_Hook_Manager::setup();

// Check if admin
        if (is_admin()) {
// Setup Settings
            $settings = new WC_Konduto_Settings();
            $settings->setup();
        }
    }

}

function __woocommerce_konduto() {
    new WooCommerce_Konduto();
}

// Create object - Plugin init
add_action('plugins_loaded', '__woocommerce_konduto');

function konduto_footer() {
// Load the settings.
// Options.
    $public_key = get_option('wc_settings_wc_konduto_publickey');

    $current_user = wp_get_current_user();

    $user_id = $current_user->ID;
    if (isset($user_id)) {
        $userId = '__kdt.push({"customer_id": "' . $user_id . '"});';
    }

    echo '
    <script type="text/javascript">
        var __kdt = __kdt || [];
        __kdt.push({"public_key": "' . $public_key . '"});
            ' . $userId . ';
        (function () {
            var kdt = document.createElement("script");
            kdt.id = "kdtjs";
            kdt.type = "text/javascript";
            kdt.async = true;
            kdt.src = "https://i.k-analytix.com/k.js";
            var s = document.getElementsByTagName("body")[0];
            s.parentNode.insertBefore(kdt, s);
        })();
    </script>';
}

add_action('wp_footer', 'konduto_footer', 5);

// add meta tags de acordo com a pagina
function page_specific_headers() {
    if (is_shop()) {
        echo '<script type="text/javascript"><![CDATA[<meta name="kdt:page" content="home"/>]]></script>';
    } elseif (is_product()) {
        global $product;
        echo '<script type="text/javascript"><![CDATA[<meta name="kdt:page" content="product"><meta property="kdt:product" content="sku=' . $product . ',name=' . get_the_title() . '"/>]]></script>';
    } elseif (is_product_category()) {
        echo '<script type="text/javascript"><![CDATA[<meta name="kdt:page" content="category"/>]]></script>';
    } elseif (is_lost_password_page()) {
        echo '<script type="text/javascript"><![CDATA[<meta name="kdt:page" content="password_reset"/>]]></script>';
    } elseif (is_cart()) {
        echo '<script type="text/javascript"><![CDATA[<meta name="kdt:page" content="basket"/>]]></script>';
    } elseif (is_checkout()) {
        echo '<script type="text/javascript"><![CDATA[<meta name="kdt:page" content="checkout"/>]]></script>';
    } elseif (is_account_page()) {
        echo '<script type="text/javascript"><![CDATA[<meta name="kdt:page" content="account"/>]]></script>';
    } elseif (is_lost_password_page()) {
        echo '<script type="text/javascript"><![CDATA[<meta name="kdt:page" content="password_reset"/>]]></script>';
    }
}

add_action('wp_head', 'page_specific_headers', 5);

function plugin_add_settings_link($links) {
    $settings_link = '<a href="admin.php?page=wc-settings&tab=wc_konduto">' . __('Settings') . '</a>';
    array_push($links, $settings_link);
    return $links;
}

$plugin = plugin_basename(__FILE__);
add_filter("plugin_action_links_$plugin", 'plugin_add_settings_link');

/**
 * Envia o pedido para o Konduto
 */
add_action('woocommerce_checkout_update_order_meta', 'konduto_checkout_sent_order');

function konduto_checkout_sent_order($order_id) {
    $order = new WC_Order($order_id);
    
}

/**
 * Do requests in the Konduto API.
 *
 * @param  string $url      URL.
 * @param  string $method   Request method.
 * @param  array  $data     Request data.
 * @param  array  $headers  Request headers.
 *
 * @return array            Request response.
 */
function do_request($url, $method = 'POST', $data = array(), $headers = array()) {
    $params = array(
        'method' => $method,
        'timeout' => 60,
    );

    if ('POST' == $method && !empty($data)) {
        $params['body'] = $data;
    }

    if (!empty($headers)) {
        $params['headers'] = $headers;
    }

    return wp_safe_remote_post($url, $params);
}
